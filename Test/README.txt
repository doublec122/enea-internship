Here I test both programs to illustrate both correct outputs and errors. 

*- In both projects, I have outputs in text files (called Output.txt, and OutputError.txt, respectively) in which you can inspect the output from the main.cpp file, as well as the main.cpp file, which is present in both cases next to the output file. 

    - In the case of the allocators, I made tests with Valgrind, in order to verify that they work properly. The different outputs from Valgrind are saved in the same fashion as the Output files in the Matrix project (same names for output files, plus main.cpp next to each output file).

    - An important note is, that in the case of the first allocator (in the V1 directory), there is no error output file, since in that particular implementation, the resources are automatically released when the allocator object is destroyed, so, from the sole modification of main.cpp, it is not possible to create leaks. 

