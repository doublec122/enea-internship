#include <iostream>
#include "matrix.hpp"

int main() {
    
    Matrix matInv (0, 2, { {4, 7}, {2, 6} });

    Matrix m(-2, 2);
    m.printMatrix();

    matInv.inverse();

    return 0;
}

