#include <iostream>
#include "matrix.hpp"

int main() {

    Matrix mat (4, 4, { {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1} });
    Matrix mat2 (4, 4, { {2, -2, 2, 2}, {2, 2, 2, 2}, {2, 2, 2, 2}, {2, 2, 2, 2} });
    Matrix mat5 (4, 2, { {1, -12}, {5, 66}, {9, 6}, {7, 19}});
    Matrix matt (1, 1, { {45} });
    Matrix matInv (2, 2, { {4, 7}, {2, 6} });
    Matrix matInv2 (5, 5, { {12, -2, 1, 6, 4}, {10, 7, 43, 11, -8}, {3, 9, 32, 78, 6}, {112, -3, -23, -1, -90},
                    {8, -12, 2, 2, 29} });

    Matrix m(2, 2);
    m = matt;
    m.printMatrix();

    matInv.inverse();
    matInv2.inverse();

    Matrix mat3 = mat2 + mat2;
    mat3 = mat + mat + mat2 + mat * mat + mat2 * mat2;
    mat3.printMatrix();

    Matrix mat4 = mat * mat5;
    mat4.printMatrix();

    mat4 = mat2;
    mat4.printMatrix();

    Matrix MAT(4, 4);
    MAT.printMatrix();
    MAT = mat5;

    mat5.printMatrix();
    mat5.transpose();
    mat5.printMatrix();

    mat5 = mat;
    mat5.printMatrix();
    mat5 = matt + matt;
    mat5 = mat2 * mat2 * mat;

    Matrix mat6 (4, 4, {{2, 2, 2, 2}, {2, 2, 2, 2}, {2, 2, 2, 2}, {2, 2, 2, 2}});

    mat2.transpose();
    mat5 = mat3;
    mat5.transpose();
    Matrix MAT2(2, 2);
    mat5 = mat3 + mat3 * mat3;

    Matrix mat7 (4, 2, {{1, 12}, {5, 66}, {9, 6}, {7, 19}});
    mat7.transpose();
    mat7.printMatrix();
    mat2 = mat3 * mat3;

    return 0;
}

