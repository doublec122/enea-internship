==25992== Memcheck, a memory error detector
==25992== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==25992== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==25992== Command: ./main
==25992== 
==25992== 
==25992== HEAP SUMMARY:
==25992==     in use at exit: 32 bytes in 1 blocks
==25992==   total heap usage: 5 allocs, 4 frees, 72,816 bytes allocated
==25992== 
==25992== LEAK SUMMARY:
==25992==    definitely lost: 32 bytes in 1 blocks
==25992==    indirectly lost: 0 bytes in 0 blocks
==25992==      possibly lost: 0 bytes in 0 blocks
==25992==    still reachable: 0 bytes in 0 blocks
==25992==         suppressed: 0 bytes in 0 blocks
==25992== Rerun with --leak-check=full to see details of leaked memory
==25992== 
==25992== For counts of detected and suppressed errors, rerun with: -v
==25992== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
 
