#include "allocator.hpp"
#include <string.h>

int main () {
	
	
	
	Allocator<char> allocator(32);
	Allocator<char> alloc(64);
	
	char* var = allocator.alloc();
	char* var2 = allocator.alloc();
	char* var3 = allocator.alloc();
	
	strcpy(var, "Hello, World!");
	
	std::cout << var << "\n";
	
	allocator.free(var);
	allocator.free(var2);
	allocator.free(var3);
	//alloc.free();
	
	return 0;
}
