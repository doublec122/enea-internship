#ifndef ALLOCATOR_H
#define ALLOCATOR_H

#include <iostream>
#include <assert.h>
#include <cstdint>
#include <stdint.h>
#include <stdlib.h>  
#include <vector>

template <class T>

class Allocator {
	
	struct Node {
		
		T data;
        Node*   next;
    };
	
	std::vector<Node*> freeList; // List (actually vector) will hold the chunks.
	Node* head = nullptr;
	size_t maxElems = 0; // Keeps track of the pool size
	
public:

	Allocator(Allocator const&) = delete;
    Allocator& operator=(Allocator const&) = delete;
	
	Allocator (const size_t _poolSize) { // The head points to the start of the newly allocated pool
		
		head = allocChunk(_poolSize);
	}
	
	Allocator(Allocator&& o) : freeList{ std::move(o.freeList) }, head{ o.head }
	, maxElems{ o.maxElems } {}

	Allocator& operator=(Allocator&& o) {
		
        /*for (auto n : freeList) {
            std::free(n);
        }*/
        
        freeList = std::move(o.freeList);
        head = o.head;
        maxElems = o.maxEelems;

        return *this;
    }
    
    ~Allocator () {
		
		for (auto n : freeList) std::free(n);
	}
	
	operator bool() const {
		
		return freeList.size();
	}
	
	T* alloc () { // Get a chunk from the pool
		
		if (!head) { // If the pool is full, we allocate another one.
			
			head = allocChunk(maxElems);
			
			if (!head) return nullptr;
		}
		
		auto _head = head;
		head = head->next;
		return &_head->data;
	}
	
	void free (T* ptr) { // Return a chunk to the pool
		
		if (!ptr) return;
		
		size_t* mem_raw = reinterpret_cast<size_t*> (ptr);
		//mem_raw -= offsetof(Node, data);
		Node* mem_head = reinterpret_cast<Node*>(mem_raw);
        mem_head->next = head;
        head = mem_head;
	}
	
private:
	
	Node* allocChunk (size_t poolSize) { // Here we make the allocation for the big chunk, and then initialize the vector.
		
		uint64_t alloc_sz = sizeof(Node) * poolSize;
		
		Node* m = reinterpret_cast<Node*>(std::malloc (alloc_sz));
		
		if (!m) {
			
            return nullptr;
        }
		
		freeList.push_back(m);
		Node* it = m;
		
		for (uint64_t i = 1; i < poolSize; ++i, ++it) {
            it->next = it + 1;
        }
        
        it->next = nullptr;
        maxElems += poolSize;
        
        return m;
	}
};

#endif
