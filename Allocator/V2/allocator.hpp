#include <iostream>
#include <vector>

template <class T>

class Allocator {
	
    const size_t poolSize = 0; // Size of the pool
    std::vector<T *> freeVec; // Vector of chunks
    size_t next_pos;

    void alloc_pool(size_t poolSize) { // Allocate pool 
		
        next_pos = 0;       
        void *temp = operator new(poolSize);
        freeVec.push_back(static_cast<T *>(temp)); // Push the chunks into the vector
    }

public:

    Allocator(size_t _poolSize) : poolSize(_poolSize) {
		
		alloc_pool(poolSize);
    }

    T* operator()(T const &x) {
		
        if (next_pos == poolSize)
            alloc_pool(poolSize);

        T *ret = new(freeVec.back() + next_pos) T(x);
        ++next_pos;
        
        return ret;
    }
    
    void free () { // Release all the memory
		
		while (!freeVec.empty()) {
			
            T *p = freeVec.back();
            operator delete(static_cast<void *>(p));
            freeVec.pop_back();
        }
	}
}; 
