The project contains the two assignments, the Matrix and the Allocator. In the case of the Allocator, I made two versions, one in which I use a Node struct, with resource release in the destructor and individual free, and a simpler one, in which I don't use a struct Node and all resources are released when the object is destroyed.

The allocators I implemented are pool types, and they usually work by allocating a big chunk (pool) of memory initially, from which the program will allocate and deallocate resources. The big data pool is splitted into same size chunks, which are structurally linked together in a linked list fashion, and so although the initial pool allocation and list initialization is O(n), allocation and deallocation after that is quite fast, in O(1). Memory fragmentation is also greatly reduced.

Overall there are multiple types of allocators, that are usually selected based on their restrictions and on how they suit in the particular application. Apart from the pool allocators, we can have linear allocators (memory allocated in a sequential fashion, but can't deallocate individually), stack allocators (similar to linear ones, but as the name implies, they use a LIFO way of allocation/deallocation), buddy allocators (Data is organized exponentially in powers of two)etc. 

Compilation of both projects is done with g++ like so: 
g++ -o outfile *.cpp
