#include <iostream>
#include "matrix.hpp"

Matrix::Matrix (int rows_, int columns_, std::vector<std::vector<double>> values) 
: rows{rows_}, columns{columns_} {
    
    std::vector<double> container;

    if ( !checksign (rows, columns) ) { std::cout << "[Matrix Object] : Matrix must be at least 1 x 1.\n"; return; }
    if ( !matcheck (values) ) { std::cout << "[Matrix Object] : Matrix sizes don't match\n"; return; }
        
    for (std::vector<double> intvec : values) {
        
        for ( int value : intvec) {
            
            container.push_back(value);
        }
        
        vals.push_back(container);
        container.clear();
    }
}

Matrix::Matrix (int rows_, int columns_) : rows{rows_}, columns{columns_} {
    
    std::vector<double> container;
    
    if ( !checksign (rows, columns) ) { std::cout << "[Matrix Object] : Matrix must be at least 1 x 1.\n"; return; }
    
    for ( int row = 0; row < rows_; ++row ) {
        
        for ( int cols = 0; cols < columns_; ++cols ) {
            
            container.push_back(0);
        }
        
        vals.push_back(container);
        container.clear();
    }
}

Matrix::~Matrix() {
    
}

const Matrix Matrix::operator + (const Matrix& mat) const {
    
    if ( !(rows == mat.rows && columns == mat.columns) ) {
        
        std::cout << "[Addition] : For addition the matrices must be the same size.\n"; 
        exit(-1); 
    }
    
    if (vals.size() == 0 || mat.vals.size() == 0) { 
        
        std::cout <<"[Addition] : One or both matrices don't have initialized elements\n"; 
        exit (-1); 
    }
    
    Matrix res (mat.rows, mat.columns);
    
    for ( int row = 0; row < rows; ++row ) {
        
        for ( int col = 0; col < columns; ++col ) {
            
            res.vals[row][col] += this->vals[row][col] + mat.vals[row][col];
        }
    }
    
    return res;
}

const Matrix Matrix::operator * (const Matrix& mat) const {

    if ( columns != mat.rows ) { 
        
        std::cout<<"[Multiplication] : For multiplication the matrix A's columns must be the same number as matrix B's rows\n"; 
        exit(-1);
    }
    
    if ( vals.size() == 0 || mat.vals.size() == 0 ) { 
        
        std::cout <<"[Multiplication] : One or both matrices don't have initialized elements\n"; 
        exit (-1); 
    }
    
    Matrix res (rows, mat.columns);
    
    for ( int row = 0; row < rows; ++row ) {
        
        for ( int col = 0; col < mat.columns; ++col ) {
            
             int sum = 0;
            
            for ( int k = 0; k < columns; ++k ) {
                
                sum = sum + (vals[row][k] * mat.vals[k][col]);
            }
            
            res.vals[row][col] = sum;
        }
    }
    
    return res;
}

Matrix &Matrix::operator = (const Matrix& mat) {
    
    if ( vals.size() == 0 || mat.vals.size() == 0 ) { 
        
        std::cout <<"[Assignation] : One or both matrices don't have initialized elements\n"; 
        exit (-1); 
    }
    
    rows = mat.rows;
    columns = mat.columns;
    vals = mat.vals;
    
    return *this;
}

bool Matrix::operator == (const Matrix& mat) const {
    
    if ( vals.size() == 0 || mat.vals.size() == 0 ) { 
        
        std::cout <<"[Equivalence] : One or both matrices don't have initialized elements\n"; 
        exit (-1); 
    }
    
    if ( rows != mat.rows || columns != mat.columns ) return 0;
    else {
        
        for ( unsigned int row = 0; row < rows; ++row ) {
            
            for ( unsigned int col = 0; col < columns; ++col ) {
                
                if ( this->vals[row][col] != mat.vals[row][col] ) return 0;
            }
        }
    }
    
    return 1;
}

void Matrix::transpose () {
    
    Matrix res (rows, columns);
    res = *this;
    this->changeSize (columns, rows);
    
    for ( int row = 0; row < rows; ++row) {
        
        for ( int col = 0; col < columns; ++col) {
            
            this->vals[row][col] = res.vals[col][row];
        }
    }
}

void Matrix::inverse () {
    
    if ( this->rows != this->columns ) { std::cout << "[Inverse] : The determinant cannot be calculated.\n"; return; }    
    
    Matrix A = *this;
    
    if ( getDet(A.rows, A.vals) == 0 ) { std::cout << "[Inverse] : The matrix has no inverse (det == 0).\n"; return; }
    
    for ( unsigned int row = 0; row < A.rows; ++row ) {
        
        for ( unsigned int col = 0; col < A.columns; ++col ) {
            
            A.vals[row][col] = this->vals[row][col];
        }
    } 

    double det = getDet(A.rows, A.vals);
    //double f = A.rows;
    
    std::vector<std::vector<double>> b (A.rows, std::vector<double>(A.rows));
    std::vector<std::vector<double>> fac (A.rows, std::vector<double>(A.rows));
    std::vector<std::vector<double>> transpose (A.rows, std::vector<double>(A.rows));
    std::vector<std::vector<double>> inverse (A.rows, std::vector<double>(A.rows));
    
    int p, q, m, n, i, j;
    
    for ( q = 0; q < A.rows; q++ ) {
        
        for ( p = 0; p < A.rows; p++ ) {
            
            m = 0;
            n = 0;
            
            for ( i = 0; i < A.rows; i++ ) {
                
                for ( j = 0; j < A.rows; j++ ) {
                    
                    if ( i != q && j != p ) {
                        
                        b[m][n] = A.vals[i][j];
                        
                        if ( n < (A.rows - 2) ) n++;
                        else {
                            
                            n = 0;
                            m++;
                        }
                    }
                }
            }
            
            fac[q][p] = pow(-1, q + p) * getDet(A.rows - 1, b);
        }
    }
    
    for (int row = 0; row < A.rows; ++row ) {
        
        for ( int col = 0; col < A.rows; ++col ) {
            
            transpose[row][col] = fac[col][row];
        }
    }
    
    for ( int row = 0; row < A.rows; ++row ) {
        
        for ( int col = 0; col < A.rows; ++col ) {
            
            inverse[row][col] = transpose[row][col] / det;
        }
    }
    
    for ( int row = 0; row < A.rows; ++row ) {
        
        for ( int col = 0; col < A.rows; ++col) {
            
            std::cout << inverse[row][col] << " ";
        }
        
        std::cout<<"\n";
    }
}

int Matrix::matcheck (std::vector<std::vector<double>> values) {
    
    int matcheck = 0;
    
    for ( std::vector<double> intvec : values ) {
        
        for ( int value : intvec ) {
            
            matcheck++;
        }
    }
    
    if (matcheck != (this->rows * this->columns)) return 0;
    else return 1;
}

int Matrix::checksign (int rows, int columns) {
    
    if (rows < 1 || columns < 1) return 0;
    else return 1;
}

double Matrix::getDet (int n, std::vector<std::vector<double>> mat) {
    
    if (n == 1) {
        
        return mat[0][0]; 
        
    } else {
        
        double det = 0;
        int i, row, col, j_aux, i_aux;
        
        for( i = 0; i < n; i++ ) {
            
            if ( mat[0][i] != 0 ) {
                
                std::vector<std::vector<double>> aux (n-1, std::vector<double>(n-1));
                i_aux = 0;
                j_aux = 0;
                
                for (row = 1; row < n; row++) {
                    
                    for (col = 0; col < n; col++) {
                        
                        if (col != i) {
                            
                            aux[i_aux][j_aux] = mat[row][col];
                            j_aux++;
                        }
                    }
                    i_aux++;
                    j_aux = 0;
                }
                double factor = (i % 2 == 0) ? mat[0][i] : -mat[0][i];                
                det = det + factor * getDet(n - 1, aux);
            }
        }
        return det;
    }
}

void Matrix::changeSize (int rows, int cols) {
    
    if ( !checksign (rows, cols) ) { std::cout << "[Size] : Matrix must be at least 1 x 1.\n"; return; }
    
    vals.clear();
    
    this->rows = rows;
    columns = cols; 
    
    std::vector<double> container;
    
    for ( int row = 0; row < this->rows; ++row ) {
        
        for ( int cols = 0; cols < columns; ++cols ) {
            
            container.push_back(0);
        }
        
        vals.push_back(container);
        container.clear();
    }
}

void Matrix::setValues (std::vector<std::vector<double>> values) {
    
    if ( !matcheck(values) ) { std::cout << "[Set] : Matrix sizes don't match\n"; return; };
    
    std::vector<double> container;
    this->vals.clear();
    
    for ( std::vector<double> intvec : this->vals ) {
        
        for ( double value : intvec ) {
            
            container.push_back(value);
        }
        
        this->vals.push_back(container);
        container.clear();
    }
}

void Matrix::printMatrix () {
    
    if ( vals.size() == 0 ) { 
        
        std::cout <<"[Print] : One or both matrices don't have initialized elements\n"; 
        return; 
    }
    
    for ( unsigned int row = 0; row < rows; ++row ) {
        
        for ( unsigned int col = 0; col < columns; ++col ) {
            
            std::cout << this->vals[row][col] << " ";
        }
        
        std::cout<<"\n";
    }
    
    std::cout << "\n";
}
