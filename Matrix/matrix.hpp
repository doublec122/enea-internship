#include <iostream>
#include <vector>
#include <math.h>

class Matrix {
    
    int columns;
    int rows;
    std::vector<std::vector<double>> vals;
    
    int matcheck (std::vector<std::vector<double>>); // Used when creating or changing values of a matrix. Checks that the rows x cols match the number of values entered.
    int checksign (int, int); // Checks that matrix is at least 1x1.
    double getDet (int, std::vector<std::vector<double>>); // Calculates the determinant.
    
public:
    Matrix (int, int, std::vector<std::vector<double>>);
    Matrix (int, int); // Matrix is filled with 0s.
    ~Matrix();
    const Matrix operator + (const Matrix&) const;
    const Matrix operator * (const Matrix&) const;
    Matrix &operator = (const Matrix&);
    bool operator == (const Matrix&) const;
    
    void inverse ();
    void transpose ();
    void setValues (std::vector<std::vector<double>>); 
    void changeSize (int, int);
    void printMatrix ();
};
